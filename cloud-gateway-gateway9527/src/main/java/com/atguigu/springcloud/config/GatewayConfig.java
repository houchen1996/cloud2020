package com.atguigu.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author houChen
 * @date 2020/8/22 16:21
 * @Description:  gateway配置类，配置路由转发
 */
@Configuration
public class GatewayConfig {

    /*@Bean
    public RouteLocator customerRouteLocator(RouteLocatorBuilder rb){
        RouteLocatorBuilder.Builder routes = rb.routes();

        //访问 http://localhost:9527/guonei 将会转发到  http://news.baidu.com/guonei
        routes.route("path_route_atguigu",
                r -> r.path("/guonei").uri("http://news.baidu.com/guonei"))
                .route("payment_routh",
                r -> r.path("/payment/get/**").uri("http://localhost:8001/payment/get/**"));
        return routes.build();
      *//*  return null;*//*
    }*/
}
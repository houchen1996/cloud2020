package com.atguigu.springcloud.service;

import cn.hutool.core.util.IdUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.TimeUnit;

/**
 * @author houChen
 * @date 2020/8/19 14:27
 * @Description:
 */
@Service
public class PaymentService {


    public String paymentInfo_ok(Integer id) {
        int timenumber = 5;
        try {
            TimeUnit.SECONDS.sleep(timenumber);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "线程池：" + Thread.currentThread().getName() + " paymentInfo_ok,id:" + id + " O(∩_∩)O哈哈~";
    }


      @HystrixCommand(fallbackMethod = "paymentInfo_timeout_handler",
      commandProperties = {
              @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="3000") //程序运行超过3s, 就会进行服务降级
      })
    public String paymentInfo_timeout(Integer id) {
        int timenumber = 5;
        try {
            TimeUnit.SECONDS.sleep(timenumber);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "线程池：" + Thread.currentThread().getName() + " paymentInfo_timeout,id:" + id + " 耗时" + 5 + "秒！！";
    }


    public String paymentInfo_timeout_handler(Integer id) {
        return "超时+兜底+ o(╥﹏╥)o";
    }


    //服务熔断
    // 在10秒内，如果10次请求有6次是失败的，就会进行服务熔断
    @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback", commandProperties = {@HystrixProperty(name = "circuitBreaker.enabled", value = "true"),// 是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),// 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"), // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),// 失败率达到多少后跳闸
    })
    public String paymentCircuitBreaker(@PathVariable("id") Integer id) {
        if (id < 0) {
            throw new RuntimeException("******id 不能负数");
        }
        String serialNumber = IdUtil.simpleUUID();

        return Thread.currentThread().getName() + "\t" + "调用成功，流水号: " + serialNumber;
    }


    public String paymentCircuitBreaker_fallback(@PathVariable("id") Integer id) {
        return "id 不能负数，请稍后再试，/(ㄒoㄒ)/~~   id: " + id;
    }
}
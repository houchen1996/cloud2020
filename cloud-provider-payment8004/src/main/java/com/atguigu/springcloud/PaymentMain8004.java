package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author houChen
 * @date 2020/8/15 17:51
 * @Description:
 */
@SpringBootApplication
@EnableDiscoveryClient  //使用consul或者zookeeper注册中心时，向其注册服务
public class PaymentMain8004 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentMain8004.class,args);
    }
}
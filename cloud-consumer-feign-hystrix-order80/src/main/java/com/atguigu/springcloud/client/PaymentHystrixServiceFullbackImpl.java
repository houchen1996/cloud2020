package com.atguigu.springcloud.client;

import org.springframework.stereotype.Service;

/**
 * @author houChen
 * @date 2020/8/19 16:52
 * @Description:
 */
@Service
public class PaymentHystrixServiceFullbackImpl implements PaymentHystrixService {
    @Override
    public String paymentInfo_ok(Integer id) {
        return "==========PaymentHystrixServiceFullbackImpl:paymentInfo_ok==========,o(╥﹏╥)o";
    }

    @Override
    public String paymentInfo_timeout(Integer id) {
        return "==========PaymentHystrixServiceFullbackImpl:paymentInfo_timeout==========,o(╥﹏╥)o";
    }
}
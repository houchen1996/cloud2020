package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author houChen
 * @date 2020/8/19 15:18
 * @Description:
 */
@SpringBootApplication
@EnableFeignClients
//@EnableHystrix
public class ConsumerFeignHystrixMian80 {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerFeignHystrixMian80.class,args);
    }
}
package com.atguigu.springcloud.service;

/**
 * @author houChen
 * @date 2020/8/31 14:48
 * @Description:
 */
public interface IMessageProvider {

    public String send();
}

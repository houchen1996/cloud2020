package com.atguigu.springcloud.alibaba.feign;

import com.atguigu.springcloud.alibaba.domain.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author houChen
 * @date 2021/8/22 23:40
 * @Description:
 */
@FeignClient("seata-storage-service")
public interface StorageServiceClent {

    @PostMapping(value = "/storage/decrease")
    CommonResult decrease(@RequestParam("productId") Long productId,@RequestParam("count") Integer count);
}

package com.atguigu.springcloud.alibaba.feign;

import com.atguigu.springcloud.alibaba.domain.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author houChen
 * @date 2021/8/22 23:41
 * @Description:
 */
@FeignClient("seata-account-service")
public interface AccountServiceClient {

    @PostMapping(value = "/account/decrease")
    CommonResult decrease(@RequestParam("userId") Long productId, @RequestParam("count") Integer count);
}

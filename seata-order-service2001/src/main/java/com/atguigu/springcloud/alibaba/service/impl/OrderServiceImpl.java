package com.atguigu.springcloud.alibaba.service.impl;

import com.atguigu.springcloud.alibaba.dao.OrderDao;
import com.atguigu.springcloud.alibaba.domain.Order;
import com.atguigu.springcloud.alibaba.feign.StorageServiceClent;
import com.atguigu.springcloud.alibaba.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author houChen
 * @date 2021/8/22 23:36
 * @Description:
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDao orderDao;

    @Resource
    private StorageServiceClent storageServiceClent;


    @Override
    public void create(Order order) {
        log.info("---> 创建新订单");
        orderDao.create(order);

        log.info("---> 订单微服务调用库存微服务，做扣减start");
        storageServiceClent.decrease(order.getProductId(), order.getCount());
        log.info("---> 订单微服务调用库存微服务，做扣减end");

        log.info("---> 订单微服务调用账户微服务，做扣减money start");
        storageServiceClent.decrease(order.getProductId(), order.getCount());
        log.info("---> 订单微服务调用账户微服务，做扣减money end");

        log.info("---> 修改订单状态 start");
        orderDao.update(order.getUserId(), 0);
        log.info("---> 订单微服务调用账户微服务，做扣减money end");
    }
}
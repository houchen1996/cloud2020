package com.atguigu.springcloud.alibaba.service;

import com.atguigu.springcloud.alibaba.domain.Order;

/**
 * @author houChen
 * @date 2021/8/22 23:33
 * @Description:
 */
public interface OrderService {

    void create(Order order);

}

package com.atguigu.springcloud.alibaba.myhandler;

import com.atguigu.springcloud.entity.CommonResult;
import com.sun.deploy.security.BlockedException;

/**
 * @author houChen
 * @date 2021/8/18 23:54
 * @Description:
 */
public class CustomerBlockHandler {
    public static CommonResult handLerException(BlockedException exception){
        return  new CommonResult(444,"用户自定义处理，global handler");
    }
}
package com.atguigu.springcloud.alibaba.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.sun.deploy.security.BlockedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author houChen
 * @date 2021/8/6 7:51
 * @Description:
 */
@RestController
public class TestController {

    @GetMapping("testA")
    public String getA(){
        return "test a";
    }

    @GetMapping("testB")
    public String getB(){
        return "test b";
    }

    @GetMapping("/testD")
    public String testD()
    {
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        return "------testD";
    }

    @GetMapping("/testHotKey")
    @SentinelResource(value = "testHotKey",blockHandler = "del_testHotKey")
    public String testHotKey(@RequestParam(value = "p1",required = false) String p1,
                 @RequestParam(value = "p2",required = false) String p2){

        return "testHotKey";
    }

    public String del_testHotKey(String p1, String p2, BlockedException exception){
        return "========del_testHotKey,o(╥﹏╥)o";
    }
}